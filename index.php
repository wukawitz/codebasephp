<?php

date_default_timezone_set('America/New_York');

ignore_user_abort(true);

set_time_limit(60*90);

if(session_id()=='')
{
	session_start();
}

define('MVC', true);

/**
* PATHS
*/
define('ROOT', dirname(__FILE__));

define('SYSTEM_PATH', ROOT .'/system');

define('APPLICATION_PATH', ROOT .'/application');

define('CONFIG_PATH', APPLICATION_PATH .'/config');

define('CONTROLLER_PATH', APPLICATION_PATH .'/controllers');

define('MODEL_PATH', APPLICATION_PATH .'/models');

define('VIEW_PATH', APPLICATION_PATH .'/views');

define('LIBRARY_PATH', APPLICATION_PATH .'/library');

define('MEDIA_PATH', ROOT .'/media');

define('QUERY_LOG_PATH', ROOT .'/logs');

/**
* DEFAULTS
*/
define('DEFAULT_CONTROLLER', 'home');

define('DEFAULT_METHOD', 'index');

define('DEFAULT_EXTENSION', '.php');

define('DEFAULT_SEPARATOR', '/');

define('DEFAULT_LIMIT_START', '0');

define('DEFAULT_LIMIT_LENGTH', '20');

define('DEFAULT_PAGE_SIZE', DEFAULT_LIMIT_LENGTH);

define('DEFAULT_HTTPS', true);

define('DEFAULT_PATH', '');

/**
* DATABASE
*/
define('HOST', '');

define('USER', '');

define('PASS', '');

define('DBSE', '');

/**
* INCLUDE SYSTEM
*/
require_once(SYSTEM_PATH .'/includes.php');

/**
* RUN IT!
*/
Bootstrap::getInstance()->run();
