<?php

if(!defined('MVC')) die('Permission denied');

class Home extends Controller
{
	public $data = null;
	
	public $viewgroup = null;
	
	public $helper = null;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->data = $this->load->config('main');
		
		$this->viewgroup = $this->load->library('viewgroup');
		
		$this->helper = $this->load->library('helper');
	}
	
	public function index()
	{
		$this->data['page_title'] = 'Welcome';
				
		$this->viewgroup->loadViews(array('common/header', 'dashboard_home', 'common/footer'), $this->data, false, $this);
	}
}
