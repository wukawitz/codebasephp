<?php

if(!defined('MVC')) die('Permission denied');

class Form extends Base
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function  form_label($name, $for='', $required=false, $class='', $style='')
	{
		$str = '<label'. ($for ? ' for="'. $for .'"' : '') . ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>'. $name . ($required ? '<span>*</span>' : '') .'</label>';
	
		return $str;
	}
	
	public function form_hidden($name, $value='', $id='', $class='')
	{
		$str = '<input type="hidden" name="'. $name .'" value="'. $value .'"'. ($id ? ' id="'. $id .'"' : '') . ($class ? ' class="'. $class .'"' : '') .'>';
		
		return $str;
	}
	
	public function form_input($name, $value='', $size='', $class='', $style='')
	{
		$str = '<input type="text" name="'. $name .'" value="'. $value .'"'. ($size ? ' size="'. $size .'"' : '') . ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>';
		
		return $str;	
	}
	
	public function form_file($name, $size='', $class='', $style='')
	{
		$str = '<input type="file" name="'. $name .'" '. ($size ? ' size="'. $size .'"' : '') . ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>';
		
		return $str;	
	}
	
	public function form_password($name, $value='', $size='', $class='', $style='')
	{
		$str = '<input type="password" name="'. $name .'" value="'. $value .'"'. ($size ? ' size="'. $size .'"' : '') . ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>';
		
		return $str;	
	}
	
	public function form_textarea($name, $value='', $rows=5, $cols=30, $class='', $style='')
	{
		$str = '<textarea rows="'. $rows .'" cols="'. $cols .'" name="'. $name .'"'. ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>'. $value .'</textarea>';
		
		return $str;
	}
	
	public function form_dropdown($name, $optionslist, $value='', $class='', $style='')
	{
		$str = '<select name="'. $name .'"'. ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>';
		
		$options = '<option value=""></option>';
		
		foreach($optionslist as $val=>$opt)
		{
			$options .= '<option value="'. $val .'"'. ($val === $value ? ' selected' : '') .'>'. $opt .'</option>';
		}
		
		$str = $str . $options .'</select>';
		
		return $str;
	}
	
	public function form_multiselect($name, $optionslist, $values=array(), $class='', $style='')
	{
		$str = '<select name="'. $name .'"'. ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .' multiple>';
		
		$options = '<option value=""></option>';
		
		$selected_values = '';
		
		foreach($optionslist as $val=>$opt)
		{
			$options .= '<option value="'. $val .'"'. (in_array($val, (array)$values) ? ' selected' : '') .'>'. $opt .'</option>';
			
			if(in_array($val, (array)$values))
			{
				$selected_values .= '<li>'. $opt .'</li>';
			}
		}
		
		$str = $str . $options .'</select>';
		
		$str = '
			<div class="multiselect_wrap">
				<div class="multiselect_left">
					'. $str .'
					<br />
					<small>**Hint: Ctrl-click to select multiple items</small>
				</div>
				<div class="multiselect_right">
					<h6>Selected Values</h6>
					<ul>
						'. ($selected_values ? $selected_values : '<li>None</li>') .'
					</ul>
				</div>
				<div class="clear"></div>
			</div>
		';
		
		return $str;
	}
	
	public function form_checkbox($name, $value='', $checkedval='', $class='', $style='')
	{
		$str = '<input type="checkbox" name="'. $name .'" value="'. $value .'"'. ($checkedval === $value ? ' checked' : '') . ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>';
		
		return $str;	
	}
	
	public function form_radio($name, $value='', $checkedval='', $class='', $style='')
	{
		$str = '<input type="radio" name="'. $name .'" value="'. $value .'"'. ($checkedval === $value ? ' checked' : '') . ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>';
		
		return $str;
	}
	
	public function form_submit($name, $value='', $class='', $style='', $hasclear=false)
	{
		$str = '<input type="submit" name="'. $name .'" value="'. $value .'"'. ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>';
		
		if($hasclear)
		{
			$str .= '&nbsp;&nbsp;<input type="button" value="Clear"'. ($class ? ' class="'. $class .'"' : '') . ($style ? ' style="'. $style .'"' : '') .'>';
		}
		
		return $str;
	}
}