<?php

if(!defined('MVC')) die('Permission denied');

class Helper extends Base
{	
	protected $load = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->load = Load::getInstance();
	}
	
	public function headerRow($columns, $select_widths=array(), $hidden=array(), $th=false, $extracols_front=0, $extracols_end=0)
	{
		$html = '';
		
		if(!empty($columns))
		{
			$keys = array_keys($columns);
			
			if(!empty($keys))
			{				
				$html = '<tr>';
				
				if($extracols_front)
				{
					for($i=0; $i<count($extracols_front); $i++)
					{
						$html .= '<td>&nbsp;</td>';
					}
				}
				
				foreach($columns as $field=>$table)
				{
					$skip = false;
					
					if(!empty($hidden))
					{
						foreach($hidden as $hiddenfield=>$hiddentable)
						{
							if($hiddenfield == $field && $hiddentable == $table)
							{
								$skip = true;
							}
						}
					}

					if($skip)
					{
						continue;
					}
					
					$clean_field = ucwords(preg_replace('/_/', ' ', $clean_field));
				
					$current_sort = $this->getSortFromUri();
				
					if(empty($current_sort) || !isset($current_sort[$table][$field]))
					{
						$sortarray = array($table=>array($field=>'ASC'));
					}
					else
					{
						$sortarray = array($table=>array($field=>($current_sort[$table][$field] == 'ASC' ? 'DESC' : 'ASC')));
					}
					
					if(preg_match('/is_active/i', $field))
					{
						$alignment = 'align_center';
					}
					else
					{
						$alignment = 'align_left';
					}
					
					$html .= ($th ? '<th>' : '<td class="'. $alignment .'"'. (isset($select_widths[$field]) ? ' style="width:'. $select_widths[$field] .'%;"' : '') .'>') .'<a href="'. $this->buildUri(null, $sortarray) .'">'. $clean_field .'</a>'. ($th ? '</th>' : '</td>');
				}
				
				if($extracols_end)
				{
					for($i=0; $i<count($extracols_end); $i++)
					{
						$html .= '<td>&nbsp;</td>';
					}
				}
				
				$html .= '<tr>';
			}
		}
		
		return $html;
	}
	
	public function pageination($total_results, $results_per_page, $mid_show=3)
	{
		$html = '';
	
		$current_page = $this->getPageFromUri();
				
		if($total_results > $results_per_page)
		{
			$total_pages = ceil($total_results / $results_per_page);
		}
		else
		{
			$total_pages = 1;
		}
		
		$current_sort = $this->getSortFromUri();
		
		$html = '<div class="pagination">';
		
		$html .= '<span>Page '. $current_page .' of '. $total_pages .': </span>';
		
		$html .= '&nbsp;&nbsp;';
		
		if($current_page - $mid_show > 0)
		{
			$html .= '<a href="'. $this->buildUri(1, $current_sort) .'">&lt;&lt;</a>';
			$html .= '<a href="'. $this->buildUri(($current_page-1), $current_sort) .'">&lt;</a>';
		}
		
		if(($current_page - ($mid_show-1)) > 0)
		{
			$html .= '<a href="'. $this->buildUri(($current_page - ($mid_show-1)), $current_sort) .'">'. ($current_page - ($mid_show-1)) .'</a>';
		}

		if(($current_page - ($mid_show-2)) > 0)
		{
			$html .= '<a href="'. $this->buildUri(($current_page - ($mid_show-2)), $current_sort) .'">'. ($current_page - ($mid_show-2)) .'</a>';
		}
		
		$html .= '<span>'. $current_page .'</span>';
		
		if(($current_page + ($mid_show-2)) <= $total_pages)
		{
			$html .= '<a href="'. $this->buildUri(($current_page + ($mid_show-2)), $current_sort) .'">'. ($current_page + ($mid_show-2)) .'</a>';			
		}
		
		if(($current_page + ($mid_show-1)) <= $total_pages)
		{
			$html .= '<a href="'. $this->buildUri(($current_page + ($mid_show-1)), $current_sort) .'">'. ($current_page + ($mid_show-1)) .'</a>';			
		}
				
		if(($total_pages - $current_page) >= $mid_show)
		{
			$html .= '<a href="'. $this->buildUri(($current_page+1), $current_sort) .'">&gt;</a>';
			$html .= '<a href="'. $this->buildUri($total_pages, $current_sort) .'">&gt;&gt;</a>';
		}
		
		$html .= '<span class="float_right">Total Results: '. ($total_results ? $total_results : 0) .'</span>';
		
		$html .= '</div>';
		
		return $html;
	}
	
	public function buildUri($pagination=null, $sort=array())
	{
		$new_uri = '';
		
		if(isset($_SERVER['SERVER_NAME']))
		{
			$new_uri .= (DEFAULT_HTTPS ? 'https://' : 'http://'). $_SERVER['SERVER_NAME'];
		}
		
		if(isset($_SERVER['PATH_INFO']))
		{
			$current_uri = $_SERVER['PATH_INFO'];
		}
		elseif(isset($_SERVER['REQUEST_URI']))
		{
			$current_uri = $_SERVER['REQUEST_URI'];
		}
		
		$current_uri_parts = explode('/', $current_uri);

		if(!empty($current_uri_parts))
		{
			$new_uri_parts = array();
		
			foreach($current_uri_parts as $part)
			{
				if(preg_match('/^page|^sort|^success|^failure/i', $part))
				{
					continue;
				}
				elseif(!empty($_POST) && isset($_POST['search']) && preg_match('/^search/i', $part))
				{
					continue;
				}
				elseif(strlen(preg_replace('/^search-/i', '', htmlspecialchars(trim($part)))) == 0)
				{
					continue;
				}
				array_push($new_uri_parts, $part);
			}
			
			if(!empty($_POST) && isset($_POST['search']))
			{
				array_push($new_uri_parts, 'search-'. htmlspecialchars(trim($_POST['search'])));
			}
			
			if(!empty($sort))
			{
				foreach($sort as $table=>$fields)
				{
					foreach($fields as $field=>$order)
					{
						array_push($new_uri_parts, 'sort-'. $table .'!'. $field .'-'. $order);
						
						break;
					}
					
					break;
				}
			}			
			
			if($pagination)
			{
				array_push($new_uri_parts, 'page-'. $pagination);
			}
			
			if(!empty($new_uri_parts))
			{
				$new_uri .= '/'. implode('/', $new_uri_parts);
			}
		}
		
		return $new_uri;
	}
	
	public function getPageFromUri()
	{
		$current_page = 1;
	
		$current_uri = '';
	
		if(isset($_SERVER['PATH_INFO']))
		{
			$current_uri = $_SERVER['PATH_INFO'];
		}
		elseif(isset($_SERVER['REQUEST_URI']))
		{
			$current_uri = $_SERVER['REQUEST_URI'];
		}
		
		$current_uri_parts = explode('/', $current_uri);

		if(!empty($current_uri_parts))
		{
			foreach($current_uri_parts as $part)
			{
				if(preg_match('/^page/i', $part))
				{
					$current_page = preg_replace('/page-/i', '', $part);
					
					break;
				}
			}
		}
		
		return $current_page;
	}
	
	public function getSearchFromUri()
	{
		$current_search = '';
		
		$current_uri = '';
	
		if(isset($_SERVER['PATH_INFO']))
		{
			$current_uri = $_SERVER['PATH_INFO'];
		}
		elseif(isset($_SERVER['REQUEST_URI']))
		{
			$current_uri = $_SERVER['REQUEST_URI'];
		}
		
		$current_uri_parts = explode('/', $current_uri);

		if(!empty($current_uri_parts))
		{
			foreach($current_uri_parts as $part)
			{
				if(preg_match('/^search/i', $part))
				{
					$current_search = urldecode(preg_replace('/^search-/i', '', $part));
					
					break;
				}
			}
		}
		
		return $current_search;
	}
	
	public function getSortFromUri()
	{
		$sort_array = array();
		
		$sort_table = null;
		
		$sort_field = null;
		
		$sort_order = null;
		
		$current_uri = null;
	
		if(isset($_SERVER['PATH_INFO']))
		{
			$current_uri = $_SERVER['PATH_INFO'];
		}
		elseif(isset($_SERVER['REQUEST_URI']))
		{
			$current_uri = $_SERVER['REQUEST_URI'];
		}
		
		if($current_uri)
		{
			$current_uri_parts = explode('/', $current_uri);

			if(!empty($current_uri_parts))
			{
				foreach($current_uri_parts as $part)
				{
					if(preg_match('/^sort/i', $part))
					{
						$arr = explode('-', $part);
						
						if(isset($arr[1]))
						{
							$sort_table = preg_replace('/^(.*?)\!(.*)/i', "$1", trim($arr[1]));
							
							$sort_field = preg_replace('/(.*?)\!(.*)/i', "$2", trim($arr[1]));
						}
		
						if(isset($arr[2]))
						{
							$sort_order = trim($arr[2]);
						}
					}
				}

				if($sort_table && $sort_field && $sort_order)
				{
					$sort_array[$sort_table] = array($sort_field=>$sort_order);
				}
			}
		}
		
		return $sort_array;
	}
	
	public function basicUri($path)
	{
		return DEFAULT_PATH .'/'. $path;
	}
	
	public function currentSection()
	{
		$current_uri = null;
	
		if(isset($_SERVER['PATH_INFO']))
		{
			$current_uri = $_SERVER['PATH_INFO'];
		}
		elseif(isset($_SERVER['REQUEST_URI']))
		{
			$current_uri = $_SERVER['REQUEST_URI'];
		}
		
		$regex = '/'. DEFAULT_PATH .'|^\//i';
		
		$current_uri = preg_replace($regex, '', $current_uri);
		
		$uriparts = explode('/', $current_uri);
	
		if(isset($uriparts[0]))
		{
			return $uriparts[0];
		}
		
		return '';
	}
	
	public function getCurrentUri()
	{
		$current_uri = null;
	
		if(isset($_SERVER['PATH_INFO']))
		{
			$current_uri = $_SERVER['PATH_INFO'];
		}
		elseif(isset($_SERVER['REQUEST_URI']))
		{
			$current_uri = $_SERVER['REQUEST_URI'];
		}
		
		return $current_uri;
	}
	
	public function uriHasParts($partlist)
	{
		$current_uri = null;
	
		if(isset($_SERVER['PATH_INFO']))
		{
			$current_uri = $_SERVER['PATH_INFO'];
		}
		elseif(isset($_SERVER['REQUEST_URI']))
		{
			$current_uri = $_SERVER['REQUEST_URI'];
		}
				
		$uriparts = explode('/', $current_uri);
	
		$parts = explode(',', $partlist);
	
		if(!empty($uriparts))
		{
			foreach($uriparts as $uripart)
			{
				foreach($parts as $part)
				{
					if(strtolower(trim($uripart)) == strtolower(trim($part)))
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	public function sendTo($path)
	{
		header('Location: '. $this->basicUri($path));
		exit();
	}
	
	public function setMessage($type, $message)
	{
		if(!is_array($message))
		{
			$message = (array)$message;
		}
		
		$message_array = array(
			'type'=>$type,
			'message'=>$message
		);
	
		$_SESSION['infomessage'] = base64_encode(serialize($message_array));
	
		return $this;
	}
	
	public function getMessage()
	{
		$message = array();
		
		if(isset($_SESSION['infomessage']))
		{
			$message = unserialize(base64_decode($_SESSION['infomessage'])); 
		}
		
		return $message;
	}
	
	public function destroyMessage()
	{
		$_SESSION['infomessage'] = array();
		
		unset($_SESSION['infomessage']);
		
		return $this;
	}
	
	public function setSessionInfo($key, $info)
	{
		$_SESSION['info'][$key] = base64_encode(serialize($info));

		return $this; 		
	}
	
	public function getSessionInfo($key)
	{
		$data = null;
		
		if(isset($_SESSION['info'][$key]))
		{
			$data =  unserialize(base64_decode($_SESSION['info'][$key]));
		}
		
		return $data;
	}
	
	public function destroySessionInfo($key)
	{
		if(isset($_SESSION['info'][$key]))
		{
			$_SESSION['info'][$key] = null;
			
			unset($_SESSION['info'][$key]);
		}
		
		return $this;
	}
}