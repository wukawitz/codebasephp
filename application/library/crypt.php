<?php

if(!defined('MVC')) die('Permission denied');

class Crypt extends Base
{
	protected $key;
	
	protected $type;
	
	protected $salt;
	
	protected $mode;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->clean();
	}
	
	public function encrypt($data)
	{
		$ciphertext;
		
		if($this->key)
		{
			$key = pack('H*', sha1($this->key, true));
			
			if($this->type)
			{
				if($this->mode)
				{
					$iv_size = mcrypt_get_iv_size($this->type, $this->mode);
					
					$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
					
					$ciphertext = mcrypt_encrypt($this->type, $key, $data, $this->mode, $iv);

					$ciphertext = $iv . $ciphertext;
    
					$ciphertext = base64_encode($ciphertext);
				}
				else
				{
					throw new Exception('Encryption mode not set');
				}
			}
			else
			{
				throw new Exception('Encryption type not set');
			}
		}
		else
		{
			throw new Exception('Key not set');
		}
		
		return $ciphertext;
	}
	
	public function decrypt($data)
	{
		$plaintext;
		
		$ciphertext = base64_decode($data);

		if($this->key)
		{
			$key = pack('H*', sha1($this->key, true));		
		
			if($this->type)
			{
				if($this->mode)
				{
					$iv_size = mcrypt_get_iv_size($this->type, $this->mode);
					
					$iv_dec = substr($ciphertext, 0, $iv_size);
					
					$ciphertext = substr($ciphertext, $iv_size);
					
					$plaintext = mcrypt_decrypt($this->type, $key, $ciphertext, $this->mode, $iv_dec);
				}
				else
				{
					throw new Exception('Encryption mode not set');
				}
			}
			else
			{
				throw new Exception('Encryption type not set');
			}
		}
		else
		{
			throw new Exception('Key not set');
		}
		
		return $plaintext;
	}
	
	public function setKey($key)
	{
		$this->key = $key;
		
		return $this;
	}
	
	public function getKey()
	{
		return $this->key;
	}
	
	public function setEncryptionType($type)
	{
		switch($type)
		{
			case '3DES':
				$this->type = MCRYPT_3DES;
				break;
			case 'R128':
				$this->type = MCRYPT_RIJNDAEL_128;
				break;
			case 'R192':
				$this->type = MCRYPT_RIJNDAEL_192;
				break;
			case 'R256':
				$this->type = MCRYPT_RIJNDAEL_256;
				break;
			default:
				throw new Exception('Invalid encryption type');
		}
		
		return $this;
	}
	
	public function getEncryptionType()
	{
		$type;
		
		switch($this->type)
		{
			case MCRYPT_3DES:
				$type = '3DES';
				break;
			case MCRYPT_RIJNDAEL_128:
				$type = 'R128';
				break;
			case MCRYPT_RIJNDAEL_192:
				$type = 'R192';
				break;
			case MCRYPT_RIJNDAEL_256:
				$type = 'R256';
				break;
			default:
				$type = null;
		}
		
		return $type;
	}

	public function setEncryptionMode($mode)
	{
		switch($mode)
		{
			case 'CBC':
				$this->mode = MCRYPT_MODE_CBC;
				break;
			case 'OFB':
				$this->mode = MCRYPT_MODE_OFB;
				break;
			case 'CFB':
				$this->mode = MCRYPT_MODE_CFB;
				break;
			case 'ECB':
				$this->mode = MCRYPT_MODE_ECB;
				break;
			default:
				throw new Exception('Invalid encryption mode');
		}
		
		return $this;
	}
	
	public function getEncryptionMode()
	{
		$mode;
		
		switch($this->mode)
		{
			case MCRYPT_MODE_CBC:
				$mode = 'CBC';
				break;
			case MCRYPT_MODE_OFB:
				$mode = 'OFB';
				break;
			case MCRYPT_MODE_CFB:
				$mode = 'CFB';
				break;
			case MCRYPT_MODE_ECB:
				$mode = 'ECB';
				break;
			default:
				$mode = null
		}
		
		return $mode;
	}	
	
	public function setSalt($salt)
	{
		$this->salt = $salt;
		
		return $this;
	}
	
	public function getSalt()
	{
		return $this->salt;
	}
	
	public function clean()
	{
		$this->key = null;
		
		$this->type = null;
		
		$this->salt = null;
	}
}