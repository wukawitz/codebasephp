<?php

if(!defined('MVC')) die('Permission denied');

class Viewgroup extends Base
{	
	protected $load = null;

	public function __construct()
	{
		parent::__construct();
		
		$this->load = Load::getInstance();
	}
	
	public function loadViews($view_array, $data=array(), $html=false, $obj=null)
	{
		if(!empty($view_array))
		{
			$content = '';
			
			foreach($view_array as $name)
			{
				if($html)
				{
					$content .= $this->load->view($name, $data, $html, $obj);
				}
				else
				{
					$this->load->view($name, $data, $html, $obj);
				}
			}
			
			if($content)
			{
				return $content;
			}
		}
		
		return $this;
	}
}