<!DOCTYPE html>
<html>
<head>
	<title><?php echo (isset($page_title) ? $page_title .' | ' : '') . $site_title; ?></title>

	<meta name="description" content="<?php echo (isset($site_description) ? $site_description .' | ' : ''); ?>">
	<meta name="keywords" content="<?php echo (isset($site_keywords) ? $site_keywords .' | ' : ''); ?>">
	<meta charset="UTF-8">

	<link rel="stylesheet" type="text/css" href="/assets/css/reset.css">

	<script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
	
	<!--[if lt IE 9]>
	<script>
	document.createElement('header');
	document.createElement('nav');
	document.createElement('section');
	document.createElement('article');
	document.createElement('aside');
	document.createElement('footer');
	</script>
	<![endif]-->
</head>

<body>