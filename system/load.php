<?php

if(!defined('MVC')) die('Permission denied');

class Load extends Base
{
	public static $instance = null;
	
	protected $path = null;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public static function getInstance()
    {		
        if (self::$instance === null) 
		{
            self::$instance = new self();
        }
		
        return self::$instance;
    }
	
	public function controller($name)
	{
		$obj = null;
		
		$this->path = CONTROLLER_PATH . DEFAULT_SEPARATOR . strtolower($name) . DEFAULT_EXTENSION;
		
		if($this->checkFileExists($this->path))
		{
			require_once $this->path;
			
			$name = ucfirst($name);
			
			$obj = new $name;
		}
		
		return $obj;
	}
	
	public function model($name)
	{
		$obj = null;
		
		$this->path = MODEL_PATH . DEFAULT_SEPARATOR . strtolower($name) . DEFAULT_EXTENSION;
		
		if($this->checkFileExists($this->path))
		{
			require_once $this->path;
			
			$name = ucfirst($name);
			
			$obj = new $name;
		}
		
		return $obj;
	}
	
	public function view($name, $data=array(), $html=false, $obj=null)
	{
		$this->path = VIEW_PATH . DEFAULT_SEPARATOR . strtolower($name) . DEFAULT_EXTENSION;
		
		$contents = null;
		
		if($this->checkFileExists($this->path))
		{
			if(!empty($data))
			{
				foreach($data as $key=>$value)
				{
					$$key = $value;
				}
			}
			
			if($html)
			{
				ob_start();
				
				include $this->path;
				
				$contents = ob_get_contents();
				
				ob_end_clean();
			}
			else
			{
				require_once $this->path;
			}
		}
		
		if($contents)
		{
			return $contents;
		}
		
		return $this;
	}
	
	public function library($name)
	{
		$obj = null;
		
		$this->path = LIBRARY_PATH . DEFAULT_SEPARATOR . strtolower($name) . DEFAULT_EXTENSION;
		
		if($this->checkFileExists($this->path))
		{
			require_once $this->path;
			
			$name = ucfirst($name);
			
			$obj = new $name;
		}
		
		return $obj;
	}
	
	public function config($name)
	{
		$data = array();
		
		$this->path = CONFIG_PATH . DEFAULT_SEPARATOR . strtolower($name) . DEFAULT_EXTENSION;
		
		if($this->checkFileExists($this->path))
		{
			include($this->path);
		
			if(isset($config))
			{
				$data = $config;
			}
		}
		
		return $data;
	}
	
	protected function checkFileExists($path)
	{
		if(file_exists($path))
		{
			return true;
		}
		
		return false;
	}
}