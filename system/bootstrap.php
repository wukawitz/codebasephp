<?php

if(!defined('MVC')) die('Permission denied');

class Bootstrap extends Base
{
	public static $instance = null; 
	
	public $uri = null;
	
	public $controller = null;
	
	public $method = null;
	
	public $parameters = array();
	
	public function __construct() 
	{
		parent::__construct();
	}
	
	public static function getInstance()
    {		
        if (self::$instance === null) 
		{
            self::$instance = new self();
        }
		
        return self::$instance;
    }
	
	protected function getUri()
	{		
		if(isset($_SERVER['PATH_INFO']))
		{
			$this->uri = $_SERVER['PATH_INFO'];
		}
		elseif(isset($_SERVER['REQUEST_URI']))
		{
			$this->uri = $_SERVER['REQUEST_URI'];
		}
		
		return $this;
	}
	
	protected function processUri()
	{
		if($this->uri)
		{
			$uri_parts = explode('/', $this->uri);
			
			foreach($uri_parts as $part)
			{
				if(trim($part) && trim($part) != 'index.php')
				{
					$part = strtolower($part);
					
					if($this->controller == null)
					{
						$this->controller = $part;
					}
					elseif($this->method == null)
					{
						$this->method = $part;
					}
					else
					{
						if(!preg_match('/^page|^sort|^search|^excel|^pdf/i', $part))
						{
							array_push($this->parameters, $part);
						}
					}
				}
				else
				{
					continue;
				}
			}
		}
		
		if($this->controller == null)
		{
			$this->controller = DEFAULT_CONTROLLER;
		}
		
		if($this->method == null)
		{
			$this->method = DEFAULT_METHOD;
		}
        
		return $this;
	}
	
	protected function processArgv()
	{
		$this->controller = (isset($_SERVER['argv'][2]) && trim($_SERVER['argv'][2]) != '' ? strtolower(trim($_SERVER['argv'][2])) : DEFAULT_CONTROLLER);
		
		$this->method = (isset($_SERVER['argv'][3]) && trim($_SERVER['argv'][3]) != '' ? strtolower(trim($_SERVER['argv'][3])) : DEFAULT_METHOD);

		return $this;
	}
	
	protected function runController()
	{
		$filepath = CONTROLLER_PATH . DEFAULT_SEPARATOR . $this->controller . DEFAULT_EXTENSION;

		if(file_exists($filepath))
		{
			require_once($filepath);

			$controller_obj = new $this->controller;

			if(is_object($controller_obj) && method_exists($this->controller, $this->method) && !preg_match('/^_/', $this->method))
			{
				call_user_func_array(array($controller_obj, $this->method), $this->parameters);
			}
			else
			{
				throw new Exception('No class or method');
			}
		}
		else
		{
			throw new Exception('Controller file does not exist');
		}     
		
		return $this;
	}
	
	public function run()
	{		
		try
		{
			if(isset($_SERVER['argv'][1]) && trim($_SERVER['argv'][1]) == 'runcron')
			{
				$this->processArgv()->runController();
			}
			else
			{
				$this->getUri()->processUri()->runController();
			}
		}
		catch(Exception $e)
		{
			header('HTTP/1.0 404 Not Found');
			
			echo '<h1>Error 404</h1>';
			echo '<h3>'. $e->getMessage() .'</h3>';
		}
		
		return $this;
	}
}
