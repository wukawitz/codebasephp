<?php

if(!defined('MVC')) die('Permission denied');

class Model extends Base
{
	protected $db = null;
	
	protected $rowcount = null;
	
	protected $insertid = null;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->db = Database::getInstance()->connect();
		
		$this->table = strtolower(get_class($this));
	}
	
	public function getRecords($fields=array(), $wheres=array(), $orders=array(), $limit_start=null, $limit_length=null)
	{
		$results = array();
		
		if($this->table)
		{
			$this->rowcount = 0;
			
			$sql = 'SELECT ';
			
			if(!empty($fields))
			{
				$fieldsarr = array();
				
				foreach($fields as $field)
				{
					array_push($fieldsarr, $this->db->cln($this->table .'.'. $field) . ' AS '. $field);
				}
				
				if(!empty($fieldsarr))
				{
					$sql .= implode(',', $fieldsarr);
				}
			}
			else
			{
				$sql .= '*';	
			} 
			
			$sql .= ' FROM '. $this->table;
		
			if(!empty($wheres))
			{
				$wherearr = array();
				
				foreach($wheres as $key=>$value)
				{
					if(!is_array($value))
					{
						$val = (array)$value;
					}
					else
					{
						$val = $value;
					}
				
					foreach($val as $v)
					{
						if(preg_match('/^IS NULL|^IS NOT NULL/i', trim($v)))
						{
							array_push($wherearr, ' AND '. $this->db->cln($key) .' '. $this->db->cln($v));
						}
						else if(preg_match('/^IS NOT EMPTY/i', trim($v)))
						{
							array_push($wherearr, ' AND '. $this->db->cln($key) .'!=\'\'');
						}
						else
						{
							array_push($wherearr, ' AND '. $this->db->cln($key) .'=\''. $this->db->cln($v) .'\'');
						}
					}
				}
				
				if(!empty($wherearr))
				{
					$sql .= preg_replace('/^ AND/', ' WHERE', implode('', $wherearr)); 
				}
			}
		
			if(!empty($orders))
			{
				$ordersarr = array();
				
				foreach($orders as $key=>$value)
				{
					array_push($ordersarr, $this->db->cln($key) .' '. $this->db->cln($value));
				}
				
				if(!empty($ordersarr))
				{
					$sql .= ' ORDER BY '. implode(', ', $ordersarr); 
				}
			}
			
			if($limit_start != null)
			{
				$sql .= ' LIMIT '. $this->db->cln($limit_start);
				
				if($limit_length != null)
				{
					$sql .= ','. $this->db->cln($limit_length);
				}
			}

			$sql = 'SELECT * FROM ('. $sql .') AS tbl';

			$query_obj = $this->db->runQuery($sql);
		
			$this->rowcount = $query_obj->getRowCount();
		
			$results = $query_obj->getResults();
		}
		
		return $results;
	}
	
	public function insertRecord($keyvalues)
	{
		if($this->table && !empty($keyvalues))
		{
			$sql = 'INSERT INTO '. $this->table .' SET ';
			
			$insertarr = array();
			
			foreach($keyvalues as $key=>$value)
			{
				$value = $this->db->cln($value);
				
				if(preg_match('/INET_ATON/', $value))
				{
					$str = $this->db->cln($key) .'='. $value;
				}
				else
				{
					$str = $this->db->cln($key) .'=\''. $value .'\'';
				}
				
				array_push($insertarr, $str);
			}
			
			if(!empty($insertarr))
			{
				$sql .= implode(', ', $insertarr);
			}

			$query_obj = $this->db->runQuery($sql);
			
			$this->insertid = $query_obj->getInsertId();
		}
		
		return $this;
	}
	
	public function updateRecord($keyvalues, $wheres=array())
	{
		if($this->table && !empty($keyvalues))
		{
			$sql = 'UPDATE '. $this->table .' SET ';
			
			$updatearr = array();
			
			foreach($keyvalues as $key=>$value)
			{
				$value = $this->db->cln($value);
				
				$str = $this->db->cln($key) .'=\''. $value .'\'';

				array_push($updatearr, $str);
			}
			
			if(!empty($updatearr))
			{
				$sql .= implode(', ', $updatearr);
			}
			
			if(!empty($wheres))
			{
				$wheresarr = array();
				
				foreach($wheres as $key=>$value)
				{
					array_push($wheresarr, ' AND '. ($this->db->cln($key) .'=\''. $this->db->cln($value) .'\''));
				}
				
				if(!empty($wheresarr))
				{
					$sql .= preg_replace('/^ AND/', ' WHERE', implode('', $wheresarr));
				}
			}

			$query_obj = $this->db->runQuery($sql);
		
			$this->rowcount = $query_obj->getRowCount();	
		}
		
		return $this;
	}
	
	public function filterPost($post)
	{
		$return_post = array();
		
		$sql = '
			SELECT 
				column_name 
			FROM information_schema.columns 
			WHERE table_schema=\''. DBSE .'\' 
			AND table_name=\''. $this->table .'\'
		';
		
		$table_fields = $this->db->runQuery($sql)->getResults();
		
		if(!empty($table_fields))
		{
			foreach($table_fields as $fieldrow)
			{
				foreach($fieldrow as $field)
				{
					if(isset($post[$field]))
					{
						$return_post[$field] = $post[$field];
					}
				}
			}
		}
		
		return $return_post;
	}
	
	public function deleteRecord($wheres)
	{	
		if(!empty($wheres))
		{
			$sql = 'DELETE FROM '. $this->table;
		
			$wheresarr = array();
			
			foreach($wheres as $key=>$value)
			{
				array_push($wheresarr, ' AND '. ($this->db->cln($key) .'=\''. $this->db->cln($value) .'\''));
			}
			
			if(!empty($wheresarr))
			{
				$sql .= preg_replace('/^ AND/', ' WHERE', implode('', $wheresarr));
			}

			$query_obj = $this->db->runQuery($sql);
		
			$this->rowcount = $query_obj->getRowCount();
		}
	
		return $this;
	}
	
	public function getTable()
	{
		return $this->table;
	}	
	
	public function getInsertId()
	{
		return $this->insertid;
	}
	
	public function getRowCount()
	{
		return $this->rowcount;
	}
}