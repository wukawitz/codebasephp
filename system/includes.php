<?php

if(!defined('MVC')) die('Permission denied');

require_once('base.php');

require_once('bootstrap.php');

require_once('database.php');

require_once('load.php');

require_once('controller.php');

require_once('model.php');

require_once('log.php');