<?php

if(!defined('MVC')) die('Permission denied');

class Log extends Base
{
	const SESSIONVAR = 'user';
	
	public static $instance = null;
	
	protected $handle = null; 
	
	protected $logfile = null;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public static function getInstance()
    {		
        if (self::$instance === null) 
		{
            self::$instance = new self();
        }
		
        return self::$instance;
    }
	
	public function getCurrentUri()
	{
		$current_uri = null;
	
		if(isset($_SERVER['PATH_INFO']))
		{
			$current_uri = $_SERVER['PATH_INFO'];
		}
		elseif(isset($_SERVER['REQUEST_URI']))
		{
			$current_uri = $_SERVER['REQUEST_URI'];
		}
		
		return $current_uri;
	}	
	
	public function getUserIdFromCurrentSession()
	{
		$user_id = 0;
		
		if(isset($_SESSION[Log::SESSIONVAR]))
		{
			$data = unserialize(base64_decode($_SESSION[Log::SESSIONVAR]));
			
			if(isset($data['poc_ID']))
			{
				$user_id = $data['poc_ID'];
			}
		}
		
		return $user_id; 
	}
	
	public function setLogFile()
	{
		if($this->logfile == null)
		{
			$this->logfile = QUERY_LOG_PATH .'/query_log_'. date('Y_m', time()) .'.log'; 
		}
		
		return $this;
	}
	
	public function openLog()
	{
		$this->setLogFile();
		
		if($this->handle == null)
		{
			try
			{
				$this->handle = fopen($this->logfile, 'a');
			}
			catch(Exception $e){}
		}
		
		return $this;
	}
	
	public function closeLog()
	{
		if($this->handle)
		{
			try
			{
				fclose($this->handle);
			}
			catch(Exception $e){}
			
			$this->handle = null;
		}
		
		return $this;
	}
	
	public function saveToQueryLog($query)
	{
		$query = trim(preg_replace('/\s+/', ' ', preg_replace('/\n|\r|\t/', ' ', $query)));
		
		if(preg_match('/^INSERT|^UPDATE|^DELETE/i', $query))
		{
			$this->openLog();
			
			$timestamp = date('Y-m-d H:i:s', time());
			
			$url = $this->getCurrentUri();
			
			$id = $this->getUserIdFromCurrentSession();
			
			$entry = $timestamp ."\t". $id ."\t". $url ."\t\t\t". $query ."\n";
			
			$this->write($entry);
		}
		
		return $this;
	}
	
	public function write($entry)
	{
		if($this->handle)
		{
			try
			{
				fwrite($this->handle, $entry);
			}
			catch(Exception $e){}
		}
		
		return $this;
	}
}