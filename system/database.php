<?php

if(!defined('MVC')) die('Permission denied');

class Database extends Base
{
	public static $instance = null;
	
	protected $conn = null;
	
	protected $qryobj = null;
	
	protected $results = null;
	
	protected $rowcount = null;
	
	protected $insertid = null;
	
	protected $current_query = null;
	
	protected $logger = null;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->logger = Log::getInstance(); 
	}
	
	public static function getInstance()
    {		
        if (self::$instance === null) 
		{
            self::$instance = new self();
        }
		
        return self::$instance;
    }
	
	public function connect()
	{
		try
		{
			$this->conn = new PDO('mysql:host='. HOST .';dbname='. DBSE, USER, PASS);
			
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $e)
		{
			exit($e->getMessage());
		}
		
		return $this;
	}
	
	public function cln($var)
	{
		$var = addslashes(strip_tags(trim($var)));
		
		return $var;
	}
	
	public function clnIp($var)
	{
		$var = addslashes(strip_tags(trim($var)));
		
		return $var;
	}
	
	public function runQuery($query)
	{
		if($this->conn)
		{
			$this->current_query = trim($query);
			
			$this->qryobj = $this->conn->prepare($this->current_query);

			$this->qryobj->execute();
			
			if(preg_match('/^SELECT/', $this->current_query))
			{
				$this->result = $this->qryobj->fetchAll(PDO::FETCH_ASSOC);
			 
				$this->rowcount = count($this->result);
			}
			elseif(preg_match('/^INSERT/', $this->current_query))
			{
				$this->rowcount = $this->qryobj->rowCount();
				
				$this->insertid = $this->conn->lastInsertId();
			}
			else
			{
				$this->rowcount = $this->qryobj->rowCount();
			}
		}

		return $this;
	}
	
	public function getRowCount()
	{
		return $this->rowcount;
	}
	
	public function getInsertId()
	{
		return $this->insertid;
	}
	
	public function getResults()
	{
		return $this->result;
	}
	
	public function getQueryObject()
	{
		return $this->qryobj;
	}
	
	public function getCurrentQuery()
	{
		return $this->current_query;
	}
}
