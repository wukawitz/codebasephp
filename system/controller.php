<?php

if(!defined('MVC')) die('Permission denied');

class Controller extends Base
{
	public $db = null;
	
	public $load = null;

	public $helper = null;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->db = Database::getInstance()->connect();
		
		$this->load = Load::getInstance();
		
		if(!isset($_SERVER['argv'][1]) || trim($_SERVER['argv'][1]) != 'runcron')
		{
			header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
			header('Pragma: no-cache'); // HTTP 1.0.
			header('Expires: 0'); 
		}
	}
}
